package com.stringExamples;

import java.util.Arrays;

public class Anagram {

	public static void main(String[] args) {
		String s1="Roopa";
		String s2="oopar";
		
		s1=s1.toLowerCase();
		s2=s2.toLowerCase();
		
		if(s1.length() !=s2.length()) {
			System.out.println("Not an anagram");
		}
		else {
		char[] ar1 = s1.toCharArray();
		char[] ar2 = s2.toCharArray();
		
		Arrays.sort(ar1);
		Arrays.sort(ar2);
		
		System.out.println(ar1);
		System.out.println(ar2);
		
		if(Arrays.equals(ar1, ar2) == true) {
			System.out.println("Anagram");
		}
		else {
			System.out.println("Not anagram");
		}

	}
	}

}
