package com.stringExamples;

public class VowelConsonants {

	public static void main(String[] args) {
		String s= "Hello Roopa, Hiiii";
		int vCount =0;
		int cCount =0;
		s=s.toLowerCase();
		for (int i=0;i<s.length();i++) {
			if(s.charAt(i)!= ' ') {
				if(s.charAt(i)=='a' ||s.charAt(i)=='e' || s.charAt(i)=='i' || s.charAt(i)=='o' || s.charAt(i)=='u') {
					vCount=vCount+1;
				}
				else if(s.charAt(i) >= 'a' && s.charAt(i)<='z') {
					cCount=cCount+1;
				}
			}
			
		}
     System.out.println("Vowel count: "+vCount);
     System.out.println("Consonant count: "+cCount);
	}

}
