package com.collections;

import java.util.Date;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class LinkedListExample {
	
	public static void main(String [] args) {
		Predicate <String> newPred = s->s.length()>=5;
		Predicate <String> newPred2 = s->s.length()%2==0;
		System.out.println("My predicate result " + newPred.test("Roop"));
		
		/*String arr [] = {"Roopa","Mohan","game","Hello"};
		for(String i:arr) {*/
			System.out.println("And joining");
			System.out.println("My predicate result " + newPred.and (newPred2).test("Roopa"));
			System.out.println("OR joining");
			System.out.println("My predicate result " + newPred.or (newPred2).test("Roopa"));
			System.out.println("Negate");
			System.out.println("My predicate result " + newPred.negate ().test("Roopa"));
			/* } */
		
			//Function
			
	   Function<Integer,Integer> doubleit = s->2*s;
	   System.out.println("square" + doubleit.apply(5));
	   Function<Integer,Integer> cubeit = s->s*s*s;
	   System.out.println("square" + cubeit.apply(5));
	   
	   // AndThen
	   
	   System.out.println("And Then");
	   System.out.println("And then output : " + doubleit.andThen(cubeit).apply(2));
	   
       // Compose
	   
	   System.out.println("Compose");
	   System.out.println("Compose output: " + doubleit.compose(cubeit).apply(2));
	   
	   
	   //Consumer
	   
	   System.out.println("Consumer");
	   Consumer <Integer> square = s->System.out.println(s*s);
	   Consumer <Integer> doubleme = s->System.out.println(2*s);
	   square.andThen(doubleme).accept(5);
	   
	   
	   //Supplier
	   
	   System.out.println("Supplier");
	   
	   Supplier <Date> currentDate = ()->new Date();
	   System.out.println(currentDate.get());
	   
	   
	   
	   
	   //BiPredicate
	   
	   
	    System.out.println("BiPredicate");
	    BiPredicate <Integer,Integer> pre1 = (a,b)->a+b>=5;
		System.out.println("My predicate result " + pre1.test(1,2));
		
		
		//BiFunction
		System.out.println("BiFunction");
		BiFunction<Integer,Integer,Integer> multiple = (a,b)->a*b;
		   System.out.println("square" + multiple.apply(5,3));
	   
		//BiConsumer
		   System.out.println("BiConsumer");
		   BiConsumer <Integer,Integer> bisquare = (a,b)->System.out.println(a+b);
		   bisquare.accept(5,2);  
	}

}
