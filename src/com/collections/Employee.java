package com.collections;

import java.util.Comparator;

public class Employee implements Comparable<Employee> {
Integer id;
String name;
public Employee(int i, String string) {
	this.id=i;
	this.name=string;
}
public Integer getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
@Override
public String toString() {
	return "{id:"+this.id+", name:"+this.name+"}";
}

@Override
public int compareTo(Employee o) {
	return this.id-o.id;
}
public static Comparator<Employee> NameComparator = new Comparator<Employee> (){
	
	public int compare(Employee e1,Employee e2) {
		return e1.getName().compareTo(e2.getName());
	}
};

}
