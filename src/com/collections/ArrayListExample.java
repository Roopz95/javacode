package com.collections;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ArrayListExample {
	public static void main(String[] args) {
		ArrayList<Integer> arr = new ArrayList<>();
		
		
		//Add new element
		arr.add(15);
		arr.add(25);
		arr.add(32);
		arr.add(48);
		arr.add(28);
		System.out.println(arr);
		
		
		//Remove element
		arr.remove(2);
		System.out.println(arr);
		
		//Stream s = arr.stream().filter(i->i%2==0);
		//s.forEach(a->System.out.println(a));
		
		//Stream API
		//Filter
		System.out.println("Filter Example");
		arr.stream().filter(i->i%2==0).forEach( x-> System.out.println(x));
		
		//Map
		System.out.println("Map Example");
		arr.stream().map(i->i+3).forEach( x-> System.out.println(x));
		
		//Collect
		System.out.println("Collect  To list");
		arr.stream().map(i->i+3).collect(Collectors.toList()).forEach( x-> System.out.println(x));
		
		//Count
		System.out.println("Count list");
		long count = arr.stream().map(i->i+3).count();
		System.out.println(count);
		
		
		//Sorted Ascending
		
		System.out.println("Sorted list");
		arr.stream().sorted().forEach(x->System.out.println(x));
		arr.stream().sorted((i1,i2)->i1.compareTo(i2)).forEach(x->System.out.println(x));
		
		//Sorted Descending
		
		System.out.println("Sorted list Descending");
		arr.stream().filter(i->i%2==0).sorted((i1,i2)->i2.compareTo(i1)).forEach(x->System.out.println(x));
		
		
		//Minimum or Maximum
		
		System.out.println("Maximum");
		long minValue = arr.stream().max((i1,i2)->i1.compareTo(i2)).get();
		long maxValue = arr.stream().min((i1,i2)->i1.compareTo(i2)).get();
		System.out.println(minValue);
		System.out.println("Minimum");
		System.out.println(maxValue);
		
		
		//toArray()

		System.out.println ("To array method");
		Object[] a = arr.stream().sorted().toArray();
		for (Object i:a) {
			System.out.println(i);
		}
		
		//of()
		
		Integer [] newArray = {1,2,3,4};
		
		System.out.println ("of() method");
		Stream.of(newArray).sorted((i1,i2)->i2.compareTo(i1)).forEach(x->System.out.println(x));
		
		String [] stringArray = {"Roopa","All","Mohan"};
		Stream.of(stringArray).filter(r-> r.length()>=5).forEach(x->System.out.println(x));
		
		long max = Stream.of(newArray).max((i1,i2)->i1.compareTo(i2)).get();
	    System.out.println("Max "+max);
	    
	    
	    
	    
	    
	    
	    int[] ne = {1,3,2,6,4};
	    Arrays.sort(ne);
	    System.out.println(Arrays.toString(ne));
		
	    Employee [] emp = new Employee[5];
	    emp[0]=new Employee(1,"Roopa");
	    emp[1] = new Employee(3,"Mohan");
	    emp[2] = new Employee(2,"Mohan");
	    emp[3] = new Employee(6,"Ashika");
	    emp[4] = new Employee(4,"Akshaya");
	    
	    Arrays.sort(emp,Employee.NameComparator);
	    System.out.println(Arrays.toString(emp));
	    
	    
	    
	    StringBuffer abc = new StringBuffer("Hello");
	    System.out.println(abc.reverse());
	    
	    
	    String xyz = "Java";
	    
	    for(int i=xyz.length();i>0;i--) {
	    	System.out.println(xyz.charAt(i-1));
	    }
	    List<String> namesList = Stream.of(emp).map(ep->ep.getName()).collect(Collectors.toList());
	    Map<String, Long> mapOfNames = namesList.stream().collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
	    
	    Set<String> duplicates = mapOfNames.entrySet().stream().filter(i->i.getValue()>1).map(en->en.getKey()).collect(Collectors.toSet());
	    
        System.out.println(duplicates);
        
        String str = "roopalakshmi";
        
       Map<String, Long> mp =  str.chars().mapToObj(i->(char)i).collect(Collectors.groupingBy(Object::toString,Collectors.counting()));
        
        System.out.println(mp);
        
       
        // convert string to char array
        // create map with key as char and count as value
        // check if count is greater than 1 and collect those char to a set
        // print the set
		
	}
}

