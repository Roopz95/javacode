package com.collections;

import java.util.HashSet;


public class HashSetExample {
	String empName;
	Integer id ;
	 public HashSetExample(String empName,Integer id) {
	 // TODO Auto-generated constructor stub
	 this.empName = empName;
	 this.id=id;
	 }
	 public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEmpName() {
	 return empName;
	 }
	 public void setEmpName(String empName) {
	 this.empName = empName;
	 }
	 
	 public String toString(){
	 return empName;
	 }
		
		  public boolean equals(Object o)
		  { 
		  System.out.println("In equals " +"value is :" +this.id);
		  HashSetExample employee = (HashSetExample)o;
		  if(employee.getId().equals(this.id))
		  { return true; } 
		  return false;
		  }
		  
		  @Override public int hashCode() {
		  System.out.println("In hashcode "+"value is :"+this.id);
		  int hash =this.id.hashCode();
		  return hash;
		  }
		 
	 
	 public static void main(String[] args) {
		 //Custom Class Impl without override equals and hascode
		 
			/*
			 * If we notice the output we will see that hashset is allowing duplicates, but
			 * theoretically it should not allow duplicates. Then why is it allowing
			 * duplicates?
			 * 
			 * First we must understand the implementation logic behind hashset.
			 * 
			 * In hashset whenever we add the objects, before adding the object it will
			 * check the hashcode value of an object by overridding the hashcode If the
			 * calculated hashcode value is already available in the set, then it will call
			 * equals method and check the object which we are going to add is equal with
			 * the already available object in set. If the object is equal then it will not
			 * add it into the set or else it will add the object into the set.
			 */
		 
		 HashSet<HashSetExample> employees = new HashSet<>();
		 employees.add(new HashSetExample("Hi",1));
		 System.out.println("-----------------------");
		 employees.add(new HashSetExample("Roopa",2));
		 System.out.println("-----------------------");
		 employees.add(new HashSetExample("Hi",1));
		 System.out.println("-----------------------");
		 employees.add(new HashSetExample("Roopa",2));
		 System.out.println("-----------------------");
		 employees.add(new HashSetExample("learn",3));
		 System.out.println("-----------------------");
		 employees.add(new HashSetExample("from",4));
		 System.out.println("-----------------------");
		 employees.add(new HashSetExample("from",5));
		 System.out.println("-----------------------");
		 System.out.println(employees);
		 
		 
		 String s1="Roopa";
		 s1=s1 + "abc";
		 
		 
		 }
}
