package com.factoryDesignPattern;

public class FactoryObject {

	public Profession getProfession(String prof) {
		if(prof.equalsIgnoreCase("Doctor")) {
			return new Doctor();
		}
		else if(prof.equalsIgnoreCase("Engineer")) {
			return new Engineer();
		}
		else if(prof.equalsIgnoreCase("Lawyer")) {
			return new Lawyer();
		}
		return null;
	}

}
