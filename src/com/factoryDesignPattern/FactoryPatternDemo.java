package com.factoryDesignPattern;


public class FactoryPatternDemo {
	
	public static void main(String[] args) {
		FactoryObject fo = new FactoryObject();
		
		/*
		 * Scanner sc = new Scanner(System.in);
		 * System.out.println("Enter the profession:");
		 */
		String profession = "Doctor";
		
		
		Profession prof = fo.getProfession(profession);
		prof.professionType();
	}

}
