package com.factoryDesignPattern;

public class Engineer implements Profession{

	@Override
	public void professionType() {
		System.out.println("Object of Engineer class");
		
	}

}
