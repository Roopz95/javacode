package com.singletonDemo;

public class MainMethod {

	public static void main(String[] args) {
		SingletonClass s = SingletonClass.getInstance();
		SingletonClass r = SingletonClass.getInstance();
		SingletonClass t = SingletonClass.getInstance();
		
		s.print();
		r.print();
		t.print();

	}

}
