package com.singletonDemo;

public class SingletonClass {
	
	private static SingletonClass singleInstance = null;    // private static variable
	
	private SingletonClass() {  // private constructor
		
	};
	
	public static SingletonClass getInstance() {    // public static method		
		if(singleInstance == null) {
			singleInstance = new SingletonClass();
			return singleInstance;
		}
		else {
			return singleInstance;
		}
	}
	
	public void print() {
		System.out.println(singleInstance);
	}
}