package com.ExceptionsExample;

public class MyException {

	public static void main(String [] args){
		try {
			int a = 20/0;
			
			throw new ArithmeticException();
		}
		finally {
			System.out.println("Hello");
		}
	}
}
