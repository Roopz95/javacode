package com.interfaceExample;

public class Fish implements Java8Interface{

	@Override
	public void livesIn() {
	  System.out.println("Fish lives in water");
		
	}
	
	public void legs() {
		System.out.println("Fish has no legs");
	}
	
	public static void main(String [] args) {
		Fish f = new Fish();
		f.livesIn();
		f.legs();
		Java8Interface.walk();   //Static method
	}
	

}
