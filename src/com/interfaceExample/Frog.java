package com.interfaceExample;

public class Frog implements Java8Interface{


	@Override
	public void livesIn() {
		System.out.println("Frog lives in both water and land");
	}
		
	public void walk() {
		System.out.println("Frog can walk");
	}
	public static void main(String[] args){
		Frog f=new Frog();
		f.livesIn();
		f.walk();
		f.legs();
		}
}
