package abc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

//Declare and define a list of integers with duplicate values. 
//Write a program to remove the duplicate values and then sort it in descending order 
//without using java inbuilt class/ library.
public class abcd {
	
	public static void main(String args[]) {
		List<Integer> arr = new ArrayList<Integer>();
		
		arr.add(5);
		arr.add(2);
		arr.add(3);
		arr.add(3);
		arr.add(4);
		arr.add(9);
		arr.add(1);

		
		
		List<Integer> integers = List.of(1, 2, 3, 4, 5);
		 
		 Function<Integer, Integer> doubleFunction = i -> i * 2;
		 Function<Integer, Integer> square = i -> i * i;
		 
		 
		 Function<Integer, Integer> squareAndDouble = square.andThen(doubleFunction);
		 System.out.println(integers.stream()
		         .map(squareAndDouble)
		         .collect(Collectors.toList()));
		
	}
	
	//Lambda expressions
	
			public int add(int a, int b) {			
				return a+b;
			}
			
			 BiConsumer<Integer,Integer> sum =(a,b) -> System.out.println(b*b);
			 
			 
	//FI
			 
			 
			// @FunctionalInterface
			 
			// Exactly one abstract method
			 
		//	 Predicate<I>  -> 1 Input -> Return type(Boolean) -> test();
		//	 Consumer <I> -> 1 Input -> Returns nothing -> accept();
		//	 Function <I, R> -> 1 Input -> 1 Return type -> apply();
		//	 Supplier <R> -> No input -> 1 Return type -> get();
			 
			 
			 
		//	 BiPredicate<I,I> , BiConsumer<I,I>, BiFunction<I,I,R>
			 
		//	 c1, c2
			// Chaining
		//	 c1.andThen(c2)
			 
			 
	//Method Reference
			 
		//	 c1::getName()
			 
			 
	//Default and static methods in Interface
			 
	
	//Stream API
			 
			 
	
	//LocalDateTime
			
			 
	
	//Optional
	
			 
			 
	//immutable		 
			 
	//String s1="Roopa";
	//String s2 = "Roopa";
	//String s3 = new String("Roopa");
	
	
	
	//Mutable
	
	//StringBuffer  --> threadsafe
	//StringBuilder --> not threadsafe
			 
			 
	
			 
			 
			 
			
			
}
