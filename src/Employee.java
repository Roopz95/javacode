
public class Employee  
{  
 
    public static void main (String args[])  
    {  
        Engine e = new Engine();
        
        Car c = new Car(e);
        c.move();
    }  
      
}  

final class Car{

	private final Engine engine;
	 Car(Engine e) {
		this.engine=e;
	}
	 
	 public void move() 
	    {
	          
	        //if(engine != null)
	        {
	            engine.run();
	            System.out.println("Car is moving ");
	        }
	    }
}

class Engine{
	
	public void run() {
		System.out.println("Car works with engine");
	}
	
}